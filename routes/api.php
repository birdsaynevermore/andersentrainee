<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Resources\User as UserResource;

/*
|--------------------------------------------------------------------------
| API Routes
|------------------------------------------------------http://my-socnet.com/send_mail:8000-------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::put('users/{id}', 'UserController@updateUser')->name('update');
    Route::get('users', 'UserController@getUsersList')->name('getUsers');
    Route::get('users/{id}', 'UserController@getUser')->name('get');
    Route::delete('users/{id}', 'UserController@deleteUser' )->name('delete');
});

Route::post('users/register', 'UserController@register');
Route::post('users/login', 'UserController@login');
Route::post('users/password/reset_password', 'UserController@resetPassword');
Route::post('users/password/sendToken', 'UserController@sendToken');




