<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeleteUserRequest;
use App\Http\Requests\GetUserRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\SendTokenRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Mail\DeleteUserMail;
use App\PasswordReset;
use App\Services\UserService;
use App\User;
use Barryvdh\DomPDF\PDF;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ResetPasswordRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\ResetPasswordMail;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use App\Http\Resources\User as UserResource;


class UserController extends Controller
{
    protected UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function register(RegisterRequest $request)
    {
        $user = $this->userService->createUser($request->all());
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();

        return response()->json([
            'message'      => 'Successfully created user!',
            'access token' => $tokenResult->accessToken,
            'token_type'   => 'Bearer',
        ], 201);
    }

    public function login(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        if(!Auth::attempt($credentials))
        {
            return response()->json([
               'message' => 'You cannot sign with those credentials!',
                'errors' => 'Unauthorised!'
            ]);
        }

        $token = Auth::user()->createToken(config('app.name'));
        $token->token->save();

        return response()->json([
           'message'      => 'Successfully login user!',
           'access token' => $token->accessToken,
            'token_type'  => 'Bearer',
        ], 200);
    }

    public function sendToken(SendTokenRequest $request)
    {
        $email = $request->get('email');
        $user = User::where('email', $request->get('email'))->first();
        $token = Str::random(60);

        Mail::to($email)->send(new ResetPasswordMail($token));

            $passwordReset = PasswordReset::updateOrCreate(
            ['user_id' => $user->id],
            [
                'user_id' => $user->id,
                'token' => $token
             ]
        );
             return response()->json([
            'message' => 'We have e-mailed your password reset link!'
        ]);
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        $resetPassword = PasswordReset::where(
            'token',
            $request->get('token')
        )->first();

        if(Carbon::now()->diffInHours($resetPassword->created_at) >= 2) {
            $resetPassword->delete();

            return response()->json(["msg" => "Token too old create a new token!"]);
        } else {
            $user = $resetPassword->user;
            $user->password = bcrypt($request->get('password'));
            $user->save();
            $resetPassword->delete();

            return response()->json(["msg" => "Password has been successfully changed"]);
        }
    }

    public function updateUser(UpdateUserRequest $request, int $userId)
    {
        if(Auth::id() === $userId) {
            $user = Auth::user();
            $data = $this->userService->updateUser($request->all(), $user);

            return response()->json([
                'message' => 'Successfully updated user!',
            ], 200);
        } else {
            return response()->json([
               'message' => 'Permission error: you cant\'t update not your data!',
            ], 403);
        }
    }

    public function getUsersList()
    {
        $users = DB::table('users')->pluck('email');

        return response()->json([
            'users' => $users
        ], 200);
    }

    public function getUser(GetUserRequest $request, int $userId)
    {
        if(Auth::id() === $userId) {
            $user = Auth::user();

            return new UserResource($user);
        } else {
            return response()->json([
                'message' => 'Permission error: you cant\'t update not your data!',
            ], 403);
        }
    }

    public function deleteUser(DeleteUserRequest $request, int $userId)
    {
        if(Auth::id() === $userId) {
           $user = Auth::user();
           $data = $this->userService->deleteUser($request->all(), $user);

            return response()->json([
                'message' => 'Successfully user delete!',
            ], 200);
        } else {
            return response()->json([
                'message' => 'Permission error: you cant\'t delete not your data!',
            ], 403);
        }
    }
}
