<?php

namespace App\Services;
use App\Mail\DeleteUserMail;
use App\User;
//use Barryvdh\DomPDF\PDF;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Barryvdh\DomPDF\Facade as PDF;

class UserService
{
    public function createUser(array $data) : User
    {
        $data['password'] = bcrypt($data['password']);
        $user = new User;
        $user->fill($data)->save();

        return $user;
    }

    public function updateUser(array $data, User $user) : User
    {
        $data['password'] = bcrypt($data['password']);
        $user->fill($data)->save();

        return $user;
    }

    public function deleteUser(array $data, User $user) : User
    {
        $email = $user->email;
        $pdf = PDF::loadView('mails.deletuser');
        Mail::to($email)->send(new DeleteUserMail($pdf));
        $user->status = User::INACTIVE;
        $user->fill($data)->save();

        return $user;
    }
}
