<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;

class SendTokenTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->postJson(
            'api/users/password/sendToken',
            [
                'email'    => 'hlib10@mail.ru',
            ]
        )->assertStatus(Response::HTTP_OK);
    }
}
