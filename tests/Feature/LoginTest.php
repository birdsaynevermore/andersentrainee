<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->postJson(
            'api/users/login',
            [
                'email'    => 'hlib10@mail.ru',
                'password' => '123456'
            ]
        )->assertStatus(Response::HTTP_OK);
    }
}
