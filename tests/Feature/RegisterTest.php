<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
       $response = $this->json(
            'post','api/users',
            [
                'name'             => 'Sally',
                'email'            => 'dadsa@mail.ru',
                'password'         => 'password',
                'confirm_password' => 'password'
            ]
        )->assertStatus(Response::HTTP_CREATED);
    }
}
