<?php

namespace Tests\Feature;

use App\User;
use Carbon\Carbon;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Support\Str;
use Tests\TestCase;

class ResetPasswordTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $user = new User();
        $user->name = "TestUser";
        $user->email = "testmail@mail.ru";
        $user->password = "123456";
        $user->save();

        $token = Str::random(60);

        ResetPassword::createOrUpdate(
            ['user_id' => $user->id,
            'token' => $token,
            'created_at' => Carbon::now()->format('Y-m-d')]);

        $response = $this->json('post', 'api/users/password/reset_password',
            [               'token' => $token,
                          'password'=> $user->password,
                'confirme_password' => $user->password]);

        $response->assertStatus(200);
    }
}
